import 'package:flutter/material.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Container(
      alignment: Alignment.center,
      color: Colors.greenAccent,
      child: new Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          new Text(
            "First item",
            textDirection: TextDirection.ltr,
            style: new TextStyle(color: Colors.white),
          ),
          new Text(
            "Second item",
            textDirection: TextDirection.ltr,
            style: new TextStyle(color: Colors.blue),
          ),
        ],
      ),
//      child: new Text("Hello Container"]]]]],
//      textDirection: TextDirection.ltr,
//          style: new TextStyle(color: Colors.white,
//          fontWeight: FontWeight.w900),),
    );
  }
}
