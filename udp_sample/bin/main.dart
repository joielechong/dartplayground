import 'dart:convert';

import 'dart:io';

main(List<String> arguments) {
  var data = 'Hello World';

  List<int> dataToSend = Utf8Encoder().convert(data);
  int port = 3000;

  //Server
  RawDatagramSocket.bind(InternetAddress.loopbackIPv4, port)
  .then((RawDatagramSocket udpSocket) {
    udpSocket.listen((RawSocketEvent event) {
      if(event == RawSocketEvent.read) {
        Datagram dg = udpSocket.receive();
        print(Utf8Decoder().convert(dg.data));
      }
    });

    //client
    udpSocket.send(dataToSend, InternetAddress.loopbackIPv4, port);
    print('Sent!');
  });


}
