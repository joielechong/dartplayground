abstract class Car {
  bool hasWheels;
  bool hasHorn;

  void honk() => print('beep beep');
}