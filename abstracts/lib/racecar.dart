import 'car.dart';

class RaceCar extends Car {
  RaceCar() {
    this.hasWheels = true;
    this.hasHorn = true;
  }
  @override
  void honk() {
    print('honk in race car');
    super.honk();
  }


}