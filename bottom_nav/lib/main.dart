import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    home: BottomNav(),
  ));
}

class BottomNav extends StatefulWidget {
  @override
  _BottomNavState createState() => _BottomNavState();
}

class _BottomNavState extends State<BottomNav> {
  int _currentIndex = 0;

  final List<Widget> _children = [Home(), ShareFiles(), Setting()];

  void switchTabs(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Sample Bottom Navigation",
          style: TextStyle(fontSize: 30),
        ),
      ),
      body: _children[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        onTap: switchTabs,
        currentIndex: 0,
        items: [
          BottomNavigationBarItem(icon: Icon(Icons.home), title: Text('Home')),
          BottomNavigationBarItem(
              icon: Icon(Icons.attach_file), title: Text('Attach File')),
          BottomNavigationBarItem(
              icon: Icon(Icons.settings), title: Text('Setting'))
        ],
      ),
    );
  }
}

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Text('Home'),
    );
  }
}

class ShareFiles extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Text('Share Files'),
    );
  }
}

class Setting extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Text('Setting'),
    );
  }
}
