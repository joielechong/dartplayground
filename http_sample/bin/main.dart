import 'package:http/http.dart' as http;

main(List<String> arguments) async {
  var url = 'http://httbin.org';
  var response = await http.get(url);
  print('Response status ${response.statusCode}');
  print('Response body ${response.body}');

  // post
  url = 'http://httpbin.org/post';
  var postResponse = await http.post(url, body:'name=Joie&color=blue');
  print('Response status ${response.statusCode}');
  print('Response body ${response.body}');
}
