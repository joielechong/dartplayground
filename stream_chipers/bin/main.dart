import 'dart:convert';
import 'dart:math';
import 'dart:typed_data';

import 'package:collection/collection.dart';
import 'package:pointycastle/pointycastle.dart';

main(List<String> arguments) {
  final keybytes = randomBytes(16);
  final key = KeyParameter(keybytes);

  final iv = randomBytes(8);
  final params = ParametersWithIV(key, iv);

  StreamCipher cipher = StreamCipher("Salsa20");
  cipher.init(true, params);

  // Encrypt
  String plaintext = 'Hello World';
  Uint8List plain_data = createUint8ListFromString(plaintext);
  Uint8List encrypted_data = cipher.process(plain_data);

  // Decrypt
  cipher.reset();
  cipher.init(false, params);
  Uint8List decrypted_data = cipher.process(encrypted_data);

  display('Plain text', plain_data);
  display('Encrypted data', encrypted_data);
  display('Decrypted data', decrypted_data);

  // Make sure they match!
  Function eq = const ListEquality().equals;
  assert(eq(plain_data, decrypted_data));

  print(Utf8Decoder().convert(decrypted_data));
}

Uint8List createUint8ListFromString(String value) =>
    Uint8List.fromList(Utf8Encoder().convert(value));

void display(String title, Uint8List value) {
  print(title);
  print(value);
  print(Base64Encoder().convert(value));
  print('');
}

Uint8List randomBytes(int length) {
  final rnd = SecureRandom("AES/CTR/AUTO-SEED-PRNG");

  final key = Uint8List(16);
  final keyParam = KeyParameter(key);
  final params = ParametersWithIV(keyParam, Uint8List(16));

  rnd.seed(params);

  var random = Random();
  for (int i = 0; i < random.nextInt(255); i++) {
    rnd.nextUint8();
  }

  var bytes = rnd.nextBytes(length);
  return bytes;
}
