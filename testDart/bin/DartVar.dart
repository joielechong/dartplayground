void main() {
  int number = 34;
  int numberTwo = 2;

  print(number is! int);

  if(number == 34) {
    print("if true, this will run!");
  } else {
    print("else running!");
  }
}