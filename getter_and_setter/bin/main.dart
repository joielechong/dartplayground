import '../lib/animal.dart';

main(List<String> arguments) {
  Animal dog = new Animal("Rango", 6);
  dog.name = 'fiddo';
  print(dog.name);

  print('Before setter ${dog.age}');
  dog.age = 4;
  print('After setter ${dog.age}');

}
