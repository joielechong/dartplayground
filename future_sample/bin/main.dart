import 'dart:async';

void main() {
  Future<int>.delayed(
    Duration(seconds: 5),
    () {
      print("Creating the future");
      throw 'Error!';
    },
  ).then((value) {
    print(value);
  }).catchError(
    (err) {
      print('Caught $err');
    },
    test: (err) => err.runtimeType == String,
  ).whenComplete(() {
    print('Finished!');
  });
//  final myFuture = Future(() {
//    print("Creating the future");
//    return 12;
//  });

  print("Done with main()");
}
