
main(List<String> arguments) {
  // Map = key value pair
//  Map people = {'dad': 'bryan', 'son': 'Chris', 'daughter': 'Heather'};
  Map<String, String> people = new Map<String, String>();
  people.putIfAbsent('dad', () => 'Bryan');

  print(people);
  print('Keys are ${people.keys}');
  print('Values are ${people.values}');
  print('Dad is ${people['dad']}');
  print('Son is ${people['son']}');
}
