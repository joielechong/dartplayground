import '../lib/dog.dart';

main(List<String> arguments) {
  Dog dog = new Dog(6, 'Doggy');

  print('${dog.name} is ${dog.ageInDogYears()} dog years old');
}
