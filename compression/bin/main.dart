import 'dart:convert';

import 'dart:io';
import 'dart:convert';

main(List<String> arguments) {
  String data = '';
  for(int i = 0; i < 100; i++) {
    data = data + 'Hello world\r\n';
  }

  //Original data
  List original = Utf8Encoder().convert(data);
  
  // compress data
  GZipCodec gZipCodec = GZipCodec();
  List compressed = gZipCodec.encode(original);

  // decompress data
  List decompress = gZipCodec.decode(compressed);

  print('Original ${original.length} bytes');
  print('Compressed ${compressed.length} bytes');
  print('Decompressed ${decompress.length} bytes');

  String decoded = Utf8Decoder().convert(decompress);
  assert(data == decoded);

  print(decoded.length);
}
