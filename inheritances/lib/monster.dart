import 'package:inheritances/dragon.dart';
import 'package:inheritances/feline.dart';
import 'package:inheritances/ghost.dart';

class Monster extends Feline with Dragon, Ghost {
  bool glowInDark = true;
  @override
  void test() {
    print('Test called in monster');
    super.test();
  }
}