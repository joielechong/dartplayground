import 'package:inheritances/dragon.dart';
import 'package:inheritances/mammal.dart';

class Feline extends Mammal {
  bool hasClaws = true;

  void growl() => print('grrrr');

  @override
  void test() {
    print('testing in feline');
    super.test();
  }
}