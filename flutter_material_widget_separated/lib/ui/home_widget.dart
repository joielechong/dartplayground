import 'package:flutter/material.dart';

class HomeWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Material(
      color: Colors.blueAccent,
      child: new Center(
        child: new Text(
          "Hello Flutter",
          textDirection: TextDirection.ltr,
          style: new TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.w800,
            fontSize: 34.5,
          ),
        ),
      ),
    );
  }
}