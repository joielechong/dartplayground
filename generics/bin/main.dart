main(List<String> arguments) {
  List<int> numbers = List<int>();
  numbers.addAll([1,2,3,4]);
  print(numbers);

  List<String> strings = List<String>();
  strings.addAll(['a', 'b', 'c']);
  print(strings);

  addNumbers<int>(1,2);
}

void addNumbers<T extends num>(T a, T b) {
  print(a + b);
}