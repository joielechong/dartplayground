import 'package:http/http.dart' as http;
main(List<String> arguments) {
  var url = "http://www.google.com";

  http.get(url).then((response) {
    print("Response status: ${response.statusCode}"); // 200 means OK
    print("Response body: ${response.body}");
  });
}
