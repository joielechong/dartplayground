
main(List<String> arguments) {
  List people = ['Joie', 'Mark', 'Kido'];
  print(people);

  for (int i = 0; i < people.length; i++) {
    print('Person at ${i} is ${people[i]}');
  }

  people.forEach((person) {
    print(person);
  });
  //or
  people.forEach((person) => print(person));
}
