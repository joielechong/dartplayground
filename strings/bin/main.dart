main(List<String> arguments) {
  String hello = 'hello world';

  String name = 'Joie lechong';
  print('Hello ${name}');

  // get a substring
  String firstName = name.substring(0, 5);
  print('firstname = ${firstName}');

  // get the index of string
  int index = name.indexOf(' ');
  String lastName = name.substring(index).trim();
  print('last name = ${lastName}');

  // length
  print(name.length);

  print(name.contains('#'));

  // Create a list
  List parts = name.split(' ');
  print(parts);
  print(parts[0]);
  print(parts[1]);
}
