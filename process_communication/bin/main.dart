import 'dart:convert';
import 'dart:io';

main(List<String> arguments) {
  Process.start('cat', []).then((Process process) {
    // transform the output
    process.stdout.transform(Utf8Decoder()).listen((data) {
      print(data);
    });

    // set the process
    process.stdin.writeln('Hello world');

    // stop the process
    Process.killPid(process.pid);

    // get the exit code
    process.exitCode.then((int code) {
      print('Exit code: ${code}');

      // exit
      exit(0);
    });
  });
}
