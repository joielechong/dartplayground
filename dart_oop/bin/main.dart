import 'computer.dart';

void main() {
  var myComputer = Computer();

  print(myComputer.brand);
  print(myComputer.gigsOfRAM);
  print(myComputer.speedInGhz);
  print(myComputer.storageSpace);

  print(myComputer.runtimeType);
}