main(List<String> arguments) {
  (){print('hello');};

  List people = ['Joie', 'Lechong', 'Burkin'];

  people.forEach(print);

  print('-------');
  people.forEach((name) {
    print(name);
  });

  print('-----------');
  people.where((name){
    switch (name) {
      case 'Joie':
        return true;
      case 'Lechong':
        return true;
      case 'Kokar':
        return true;
      default:
        return false;
    }
  }).forEach(print);
}
