import 'dart:convert';

import 'dart:io';
main(List<String> arguments) {
  int zlib = testCompress(ZLibCodec());
  int gzip = testCompress(GZipCodec());

  print('zlib = ${zlib}');
  print('gzip = ${gzip}');
}

String generateData() {
  String data = '';

  for(int i = 0; i < 1000; i++) {
    data = data + 'Hello world\r\n';
  }
  return data;
}

int testCompress(Codec codec) {
  String data = generateData();

  List original = Utf8Encoder().convert(data);
  List compressed = codec.encode(original);
  List decompressed = codec.decode(compressed);

  print('Testing ${codec.toString()}');
  print('Original ${original.length}');
  print('Compressed ${compressed.length}');
  print('Decompressed ${decompressed.length}');

  print('');

  String decoded = Utf8Decoder().convert(decompressed);
  assert(data == decoded);

  return compressed.length;
}