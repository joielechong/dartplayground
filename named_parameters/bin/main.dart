main(List<String> arguments) {
  int footage = squareFeet(length: 10, width: 5);
  print('Footage is ${footage}');
  download('myfile');
  download('myfile', port: 9090);
}

int squareFeet({int width, int length}) {
  return width * length;
}

void download(String file, {int port = 80}) {
  print('Download ${file} on port ${port}');
}
