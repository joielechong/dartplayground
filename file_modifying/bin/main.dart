import 'dart:io';

main(List<String> arguments) {
  Directory dir = Directory.current;
  print(dir.path);

  File file = File(dir.path + '/myfile.txt');
  writeFile(file);
  readFile(file);
}

void writeFile(File file) {
  RandomAccessFile raf = file.openSync(mode: FileMode.append);
  raf.writeStringSync('Hello world!\r\nHow are you today?');
  raf.flushSync();
  raf.closeSync();
}

void readFile(File file) {
  if(!file.existsSync()) {
    print('file not found!');
    return;
  }

  print('Reading string...');
  print(file.readAsStringSync());

  print('Reading bytes...');
  List values = file.readAsBytesSync();
  values.forEach((value) => print(value));
}