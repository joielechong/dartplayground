import '../lib/imports_core.dart' as mycode;
import 'dart:convert';
main(List<String> arguments) {
  mycode.sayHello();

  String myValue = 'Hello World';
  List ebytes = Utf8Encoder().convert(myValue);
  String encoded = Base64Encoder().convert(ebytes);
  print('Encoded: ${encoded}');

  List dBytes = Base64Decoder().convert(encoded);
  String decoded = Utf8Decoder().convert(dBytes);
  print('Decoded: ${decoded}');
}