main(List<String> arguments) {
  int age = 150;

  if (age == 43) print('you are 43 years old');
  if (age != 43) print('you are not 43 years old');

  if (age < 18) {
    print('You are minor');
    if (age < 13) print('You are not even a teenager');
  }

  if (age > 65) {
    print('You are a seniour');
    if (age > 102) print('You are lucky to be alive');
  }
}
